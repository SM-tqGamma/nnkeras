# NNKeras

Neural Network for signal vs background

Hopefully this works out of the box. The NNInputs are included in this directory as these are not large at the current stage and are needed for these scripts. You should do the following:

python dfcreator.py

python NN.py

You should get nice outputs

With this enviroment the script works on lxplus 'source /cvmfs/sft.cern.ch/lcg/views/LCG_95/x86_64-centos7-gcc7-opt/setup.sh'

You need to setup the TRExFitter in this directory and then it should work! 
