#!/usr/bin/env python36
import numpy as np
import h5py, sys
import tensorflow as tf
import ROOT, tqdm, root_numpy, root_pandas
import pandas as pd
#import tensorflow as tf
from array import array

inPath = "/data_ceph/harish/Unblindv1/"
outPath = "/data_ceph/harish/GFW3/Ubv1/"

nnPath = "/data/harish/chosen/"

cut = "nlep==1&&nph>0&&lep1_pt>27e3&&ph_pt>20e3&&lbj_pt>25e3&&met_met>30e3&&nbjets_DL1r_85>0&&(lepph_m<80e3||lepph_m>100e3||abs(lep1_id)!=11)"
cfgs = ["nom", "mix", "woNjets"]

Regions = ["0fj", "1fj"]
model = {}
MinMaxDF = {}
flist = {}
MinVals = {}
NormVals = {}

varlist = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', 'ph_pt', 'ph_eta', 'ph_phi', 'met_met', 'HT', 'met_phi', 'lbj_pt', 'nlep', 'nph', 'lbj_eta', 'lbj_phi', 'njets', 'fj_pt', 'fj_phi', 'fj_eta', 'nfjets', 'blep_rap', 'topph_ctheta', 'transMass', 'fjph_ctheta', 'fjet_flag', 'bfj_m', 'blep_m', 'bph_m', 'transMassWb', 'transMassWph', 'lbj_tagWeightBin_DL1r_Continuous', 'weight*', 'scale_nom', 'ph_sf*', 'lumi', 'efake*', 'hfake*', 'nbjets_DL1r_70', 'nbjets_DL1r_85', 'HLT*', 'lepph_m', 'lepph_m', 'top_m', 'fjph_deta', 'bph_deta', 'fjph_e', 'blep_deta', 'Wbsn_e', 'bph_pt', 'topph_pt', 'blep_dr', "eventNumber", "runNumber", "event_in_overlap", "ph_conversionType", "lepph_dr", "fjph_dr", "fjph_m", "lfj_dr", "truthJetMatched*", "ph_mc_production"]

flist["nom0fj"] = ['lep1_pt', 'lep1_eta', 'ph_pt', 'ph_eta', 'HT', 'lbj_eta', 'njets', 'blep_m', 'bph_m', 'transMassWb', 'transMassWph', 'lepph_dr', 'blep_dr', 'top_m', 'topph_pt', 'lbj_tagWeightBin_DL1r_Continuous']

flist["nom1fj"] = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', 'ph_phi', 'met_met', 'met_phi', 'lbj_eta', 'njets', 'fj_pt', 'fj_eta', 'nfjets', 'transMass', 'fjph_ctheta', 'fjet_flag', 'bfj_m', 'blep_m', 'transMassWb', 'lepph_dr', 'blep_dr', 'fjph_dr', 'fjph_m', 'top_m', 'Wbsn_e', 'fjph_e', 'fjph_deta']

flist["mix0fj"] = ['lep1_eta', 'lep1_id', 'ph_pt', 'ph_eta', 'met_met', 'HT', 'lbj_pt', 'lbj_eta', 'transMassWb', 'transMassWph', 'lepph_dr', 'blep_dr', 'top_m', 'topph_pt', 'lbj_tagWeightBin_DL1r_Continuous', 'bph_pt']

flist["mix1fj"] = ['lep1_pt', 'lep1_eta', 'ph_pt', 'ph_phi', 'met_met', 'HT', 'met_phi', 'lbj_pt', 'lbj_eta', 'lbj_phi', 'fj_phi', 'fj_eta', 'fjph_ctheta', 'fjet_flag', 'bfj_m', 'blep_m', 'bph_m', 'transMassWb', 'blep_dr', 'fjph_dr', 'fjph_m', 'top_m', 'Wbsn_e', 'topph_ctheta', 'fjph_e', 'fjph_deta', 'lbj_tagWeightBin_DL1r_Continuous']

flist["woNjets0fj"] = ['lep1_pt', 'lep1_eta', 'ph_eta', 'HT', 'lbj_pt', 'lbj_eta', 'transMassWph', 'lepph_dr', 'blep_dr', 'top_m', 'topph_pt', 'lbj_tagWeightBin_DL1r_Continuous']

flist["woNjets1fj"] = ['lep1_id', 'HT', 'lbj_pt', 'fj_pt', 'transMass', 'bfj_m', 'blep_m', 'transMassWph', 'lepph_dr', 'lfj_dr', 'blep_dr', 'top_m', 'fjph_e', 'fjph_deta', 'lbj_tagWeightBin_DL1r_Continuous']

NNtypes = []
for cfg in cfgs:
    for region in Regions:
        NNtype = cfg+region
        NNtypes.append(NNtype)
        model[NNtype] = tf.keras.models.load_model(nnPath+region+"_"+cfg+"/NNnet_kFold2.h5")
        MinMaxDF[NNtype] = pd.read_csv(nnPath+region+"_"+cfg+"/MinMax.csv")
        MinVals[NNtype] = np.asarray(MinMaxDF[NNtype][flist[NNtype]].values[0])
        NormVals[NNtype] = np.asarray(MinMaxDF[NNtype][flist[NNtype]].values[1]-MinMaxDF[NNtype][flist[NNtype]].values[0])


def getTreeList(inf):
    trees = []
    keys = inf.GetListOfKeys()
    for key in keys:
        obj = key.ReadObj()
        if(obj.IsA().InheritsFrom(ROOT.TTree().Class())):
            if(obj.GetName() in trees):
                continue
            else:
                trees.append(obj.GetName())
    return trees

def getDataFrame(outFName, treeName):
    #varlist=""
    rawdf = root_pandas.read_root(outFName, treeName, varlist, where=cut)
    df = rawdf.copy(deep=True)
    df.loc[df["njets"]>4, "njets"] = 4
    df.loc[df["nfjets"]>4, "nfjets"] = 4
    #print(len(df.keys()), df.keys())
    fdf = {}
    fdf["0fj"] = df[df["nfjets"]==0]
    fdf["1fj"] = df[df["nfjets"]>0]
    Y = {}
    for cfg in cfgs:
        for region in Regions:
            NNtype = cfg+region
            sdf = fdf[region][flist[NNtype]]
            ndf = (sdf-MinVals[NNtype])/NormVals[NNtype]
            X = np.asarray(ndf.values)
            if X.size==0:
                continue
            Y = model[NNtype].predict(X)
            if region == "0fj":
                rawdf.loc[rawdf["nfjets"]==0,"nnscore_"+cfg] = Y
            else:
                rawdf.loc[rawdf["nfjets"]>0,"nnscore_"+cfg] = Y

    del df
    return rawdf

def fillOTree(inFName, df, outFName):
    inf = ROOT.TFile().Open(inFName)
    intree = inf.Get("nominal")
    intree.SetBranchStatus("*", 0)
    for var in varlist:
        intree.SetBranchStatus(var,1)
    intree.SetBranchStatus("mc_generator_weights",1)
    intree.SetBranchStatus("sumWeights_vector", 1)
    outf = ROOT.TFile().Open(outFName, "UPDATE")
    otree = intree.CloneTree(0)
    nnscore_nom = array("f", [ 0 ])
    nnscore_mix = array("f", [ 0 ])
    nnscore_woNjets = array("f", [ 0 ])
    otree.Branch("nnscore_nom", nnscore_nom, "nnscore_nom/F")
    otree.Branch("nnscore_mix", nnscore_mix, "nnscore_mix/F")
    otree.Branch("nnscore_woNjets", nnscore_woNjets, "nnscore_woNjets/F")
    sel = ROOT.TTreeFormula("sel", cut, intree)
    sel.GetNdata()
    nevents = 0
    nentries = intree.GetEntries()
    for ievt in range(nentries):
        intree.GetEntry(ievt)
        proceed = sel.EvalInstance()
        if(not proceed):
            continue
        if((intree.eventNumber-df["eventNumber"].iloc[nevents]) != 0):
            print("ERROR!!! Ntuple order is different")
        nnscore_nom[0] = df["nnscore_nom"].iloc[nevents]
        nnscore_mix[0] = df["nnscore_mix"].iloc[nevents]
        nnscore_woNjets[0] = df["nnscore_woNjets"].iloc[nevents]
        otree.Fill()
        #print(otree.nnscore)
        nevents += 1
    otree.AutoSave()
    outf.cd()
    otree.Write()
    outf.Close()
    inf.Close()
    del otree, sel, intree
    return

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-f", help="Input root file name; e.g. mc16a/412147.root")
parser.add_argument("-d", help="Value for isData flag", type=bool, default=False)
parser.add_argument("-mm", help="Need MM Weights?", type=bool, default=False)
args = parser.parse_args()
fname = args.f
isData = args.d
doFake = args.mm
print("isData: ", isData)
rvList = ['weight*', 'scale_nom', 'ph_sf*', 'lumi', 'efake*', 'hfake*', 'HLT*', "event_in_overlap"]

if (isData):
    for rv in rvList:
        varlist.remove(rv)
    if (doFake):
        varlist.append("ASM_weight")
        varlist.append("lfakeweight")
        varlist.append("lfakeweight_wjetsnorm")


inf = ROOT.TFile().Open(inPath+fname)
treeList = getTreeList(inf)
if inf.nominal.GetEntries() == 0:
    print("Zero entries in the nominal tree")
    print("EXIT 0: SUCCESS!!!")
inf.Close()
#print(treeList) 
intree = None
outf = ROOT.TFile().Open(outPath+fname, "RECREATE")
outf.Close()
odf = None
for treeName in reversed(treeList):
    print(treeName)
    if treeName == "MCweights":
        continue
    if treeName != "nominal":
        continue
    odf = getDataFrame(inPath+fname, treeName)
    if (treeName == "nominal" and (not isData)):
        fillOTree(inPath+fname, odf, outPath+fname)
    else:
        odf.to_root(outPath+fname, key=treeName, mode='a') 
        del odf

print("EXIT 0: SUCCESS!!!")







