import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re, os


BIGGER_SIZE = 18
plt.rc('font', size=15)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=12)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

plt.gca().get_xaxis().get_major_formatter().set_scientific(True)
plt.gca().get_yaxis().get_major_formatter().set_scientific(True)

#df.dropna(how="any")   # Dropping NaN

def binaryHist(df, var, bins, xlo, xhi, xlbl, fname, density=False, opath=""):
    os.system("mkdir -p %s" %opath)
    procList = df.groupby("category")
    plt.figure(figsize=(10,10))
    legs = ["Background", "Signal"]
    x = []
    wgts = []
    for grp in [0,1]:
        dfg = procList.get_group(grp)
        x.append(dfg[var].values)
        wgts.append(dfg["mcWeight"].values)
    ax = plt.hist(x, density=density,bins=bins, range=(xlo, xhi), weights= wgts,histtype='step', label=legs, linewidth=3)
    handles, labels = plt.gca().get_legend_handles_labels()
    plt.legend(loc='best', ncol=2)
    plt.xlabel(xlbl)
    plt.ylabel("Events/bin")
    bottom, top = plt.ylim()
    plt.savefig(opath+fname+"v1.pdf", bbox_inches='tight')
    plt.yscale("log")
    plt.savefig(opath+fname+"_logv1.pdf", bbox_inches='tight')
    plt.close()


def stackHist(df, var, bins, xlo, xhi,xlbl, fname, opath="", include_weights=True):
    os.system("mkdir -p %s" %opath)
    procList = df.groupby("procName")
    plt.figure(figsize=(10,10))
    legs = ["$Z$ + jets", "$W$ + jets", "t-chan", "s-chan", "VV", r"$t\bar{t}$", "$Z\gamma$", "$W\gamma$", r"$t\bar{t}\gamma$", "$tq\gamma$"]
    cols = [(204.0/255, 102.0/255, 255.0/255),(0,95.0/255,134.0/255),(214.0/255,210.0/255,196.0/255),(156.0/255,173.0/255,183.0/255),(238.0/255,31.0/255,96.0/255),(1,0,0),(0,169.0/255,183.0/255),(255.0/255,214.0/255,0),(87.0/255,157.0/255,66.0/255),(248.0/255,151.0/255,31.0/255)]
    x = []
    wgts = []
    for grp in range(10,0, -1):
        dfg = procList.get_group(grp)
        x.append(dfg[var].values)
        wgts.append(dfg["mcWeight"].values)
    if(not include_weights):
        wgts = None
    ax = plt.hist(x, stacked=True, weights= wgts, label=legs, linewidth=0, range=(xlo, xhi), bins=bins, color=cols)
    handles, labels = plt.gca().get_legend_handles_labels()
    plt.legend(reversed(handles), reversed(labels), loc='best', ncol=2)
    plt.xlabel(xlbl)
    plt.ylabel("Events/bin")
    #bottom, top = plt.ylim()
    #plt.ylim(bottom, 1.15*top)
    plt.savefig(opath+fname+".pdf", bbox_inches='tight')
    plt.yscale("log")
    #plt.ylim(0.1, 1.2*top)
    plt.savefig(opath+fname+"_log.pdf", bbox_inches='tight')
    plt.close()

shortvarlist = [
("lep1_pt/1e3", 40,10,210, "Lepton $p_{T}$ [GeV]", "lep1_pt"),
("lep1_eta", 40,-4.0,4.0, "Lepton $\eta$", "lep1_eta"),
("lep1_id", 40,-20,20, "Lepton PID", "lep1_id"),
("acos(cos(lep1_phi-ph_phi))", 30,0, 3.5, "$\Delta$ $\phi$ ($\ell$, $\gamma$)", "lep1_phi"),
("ph_pt/1e3", 40,10,210, "Photon $p_{T}$ [GeV]", "ph_pt"),
("ph_eta", 40,-4.0,4.0, "Photon $\eta$", "ph_eta"),
("ph_conversionType", 10,0,10, "Photon conversion type", "ph_conv"),
("met_met/1e3", 40,10,210, "Missing $p_{T}$ [GeV]", "met_met"),
("acos(cos(met_phi-ph_phi))", 30,0, 3.5, "$\Delta$ $\phi$($\gamma$, MET)", "met_phi"),
("lj_pt/1e3", 40,10,210, "Leading jet $p_{T}$ [GeV]", "lj_pt"),
("lj_eta", 40,-5.0,5.0, "Leading jet $\eta$", "lj_eta"),
("acos(cos(lj_phi-ph_phi))",30,0, 3.5, "$\Delta$ $\phi$ ($\gamma$, leading jet)", "lj_phi"),
("lbj_pt/1e3", 40,10,210, "b-jet $p_{T}$ [GeV]", "lbj_pt"),
("lbj_eta", 40,-5.0,5.0, "b-jet $\eta$", "lbj_eta"),
("acos(cos(lbj_phi-ph_phi))", 30,0, 3.5, "$\Delta$ $\phi$ ($\gamma$, b-jet)", "lbj_phi"),
("fj_pt/1e3", 40,10,210, "Forward-jet $p_T [GeV]$", "fj_pt"),
("acos(cos((fj_phi-ph_phi)))", 30,0, 3.5, "$\Delta$ $\phi$($\gamma$, forward jet)", "fj_phi"),
("fj_eta", 40,-5.0,5.0, "Forward-jet $\eta$", "fj_eta"),
("njets", 8,-0.5,7.5, "Number of jets", "njets"),
("nfjets", 8,-0.5,7.5, "Number of forward-jets", "nfjets"),
("transMass/1e3", 50,10,310, "Transverse mass [GeV]", "transMass"),
]

varlist = [
("lep1_pt/1e3", 40,10,210, "Lepton $p_{T}$ [GeV]", "lep1_pt"),
("lep1_eta", 40,-4.0,4.0, "Lepton $\eta$", "lep1_eta"),
("lep1_id", 40,-20,20, "Lepton PID", "lep1_id"),
("lep1_phi", 30,0,3.5, "$\Delta$ $\phi$ ($\ell$, $\gamma$)", "lep1_phi"),
("ph_pt/1e3", 40,10,210, "Photon $p_{T}$ [GeV]", "ph_pt"),
("ph_eta", 40,-4.0,4.0, "Photon $\eta$", "ph_eta"),
("ph_conversionType", 10,0,10, "Photon conversion type", "ph_conv"),
("met_met/1e3", 40,10,210, "Missing $p_{T}$ [GeV]", "met_met"),
("met_phi", 30,0,3.5, "$\Delta$ $\phi$($\gamma$, MET)", "met_phi"),
("lj_pt/1e3", 40,10,210, "Leading jet $p_{T}$ [GeV]", "lj_pt"),
("lj_eta", 40,-5.0,5.0, "Leading jet $\eta$", "lj_eta"),
("lj_phi",30,0,3.5, "$\Delta$ $\phi$ ($\gamma$, leading jet)", "lj_phi"),
("lbj_pt/1e3", 40,10,210, "b-jet $p_{T}$ [GeV]", "lbj_pt"),
("lbj_eta", 40,-5.0,5.0, "b-jet $\eta$", "lbj_eta"),
("lbj_phi", 30,0,3.5, "$\Delta$ $\phi$ ($\gamma$, b-jet)", "lbj_phi"),
("fj_pt/1e3", 40,10,210, "Forward-jet $p_T [GeV]$", "fj_pt"),
("fj_phi", 30,0,3.5, "$\Delta$ $\phi$($\gamma$, forward jet)", "fj_phi"),
("fj_eta", 40,-5.0,5.0, "Forward-jet $\eta$", "fj_eta"),
("njets", 8,-0.5,7.5, "Number of jets", "njets"),
("nbjets_DL1r_85", 5,-0.5,4.5, "Number of b-jets (w.r.t. DL1r_85)", "nbjets_DL1r_85"),
("nbjets_DL1r_77", 5,-0.5,4.5, "Number of b-jets (w.r.t. DL1r_77)", "nbjets_DL1r_77"),
("lbj_tagWeightBin_DL1r_Continuous", 7,-0.5,6.5, "Leading b-jet DL1r continuous bin", "lbj_DL1r_Continuous"),
("nfjets", 8,-0.5,7.5, "Number of forward-jets", "nfjets"),
("transMass/1e3", 50,10,310, "Transverse mass [GeV]", "transMass"),
("transMassWph/1e3", 50,10,710, "3P Transverse mass lep+MET+photon [GeV]", "transMassWph"),
("transMassWb/1e3", 50,10,710, "3P Transverse mass lep+MET+b [GeV]", "transMassWb"),
("HT/1e3", 100, 10, 1510, "HT [GeV]", "ht" ),
("fjet_flag", 3, -0.5, 2.5, "Leading non b-tagged jet == forward jet", "fjet_flag"),
("top_pt/1e3", 50, 10, 310, "top quark $p_{T}$ [GeV]", "top_pt"),
("top_eta", 40, -5.0, 5.0, "top quark $\eta$", "top_eta"),
("top_phi", 30, -3.14, 3.14, "top quark $\phi$", "top_phi"),
("top_e/1e3", 100, 0, 1500, "top quark energy [GeV]", "top_e"),
("top_m/1e3", 100, 0, 500, "top quark mass [GeV]", "top_m"),
("top_dr", 50, 0, 5, "$\Delta$R($W$, $b$)", "top_dr"),
("top_dphi", 40, -3.14, 3.14, "$\Delta \phi$(W, b)", "top_dphi"),
("top_deta", 40, 0, 5, "$\Delta \eta$(W, b)", "top_deta"),
("top_ctheta", 100, -1, 1, "cos(theta)($W$, $b$)", "top_ctheta"),
("Wbsn_pt/1e3", 50, 10, 310, "W $p_{T}$ [GeV]", "Wbsn_pt"),
("Wbsn_eta", 40, -5.0, 5.0, "W $\eta$", "Wbsn_eta"),
("Wbsn_phi", 30, -3.14, 3.14, "W $\phi$", "Wbsn_phi"),
("Wbsn_e/1e3", 100, 0, 1500, "W energy [GeV]", "Wbsn_e"),
("Wbsn_m/1e3", 100, 0, 500, "W mass [GeV]", "Wbsn_m"),
("Wbsn_dr", 50, 0, 5, "$\Delta$R(lep, nu)", "Wbsn_dr"),
("Wbsn_dphi", 40, -3.14, 3.14, "$\Delta \phi$(lep, nu)", "Wbsn_dphi"),
("Wbsn_deta", 40, 0, 5, "$\Delta \eta$(lep, nu)", "Wbsn_deta"),
("Wbsn_ctheta", 100, -1, 1, "cos(theta)(lep, nu)", "Wbsn_ctheta"),
("topph_pt/1e3", 50, 10, 310, "top+photon $p_{T}$ [GeV]", "topph_pt"),
("topph_eta", 40, -5.0, 5.0, "top+photon $\eta$", "topph_eta"),
("topph_phi", 30, -3.14, 3.14, "top+photon $\phi$", "topph_phi"),
("topph_e/1e3", 100, 0, 1500, "top+photon energy [GeV]", "topph_e"),
("topph_m/1e3", 100, 0, 500, "top+photon mass [GeV]", "topph_m"),
("topph_dr", 70, 0, 7, "$\Delta$R(top, ph)", "topph_dr"),
("topph_dphi", 40, -3.14, 3.14, "$\Delta \phi$(top, ph)", "topph_dphi"),
("topph_deta", 40, 0, 5, "$\Delta \eta$(top, ph)", "topph_deta"),
("topph_ctheta", 100, -1, 1, "cos(theta)(top, ph)", "topph_ctheta"),
("Wph_pt/1e3", 50, 10, 310, "W+photon $p_{T}$ [GeV]", "Wph_pt"),
("Wph_eta", 40, -5.0, 5.0, "W+photon $\eta$", "Wph_eta"),
("Wph_phi", 30, -3.14, 3.14, "W+photon $\phi$", "Wph_phi"),
("Wph_e/1e3", 100, 0, 1500, "W+photon energy [GeV]", "Wph_e"),
("Wph_m/1e3", 100, 0, 500, "W+photon mass [GeV]", "Wph_m"),
("Wph_dr", 50, 0, 5, "$\Delta$R(W, ph)", "Wph_dr"),
("Wph_dphi", 40, -3.14, 3.14, "$\Delta \phi$(W, ph)", "Wph_dphi"),
("Wph_deta", 40, 0, 5, "$\Delta \eta$(W, ph)", "Wph_deta"),
("Wph_ctheta", 100, -1, 1, "cos(theta)(W, ph)", "Wph_ctheta"),
("lepph_pt/1e3", 50, 10, 310, "lep+photon $p_{T}$ [GeV]", "lepph_pt"),
("lepph_eta", 40, -5.0, 5.0, "lep+photon $\eta$", "lepph_eta"),
("lepph_phi", 30, -3.14, 3.14, "lep+photon $\phi$", "lepph_phi"),
("lepph_e/1e3", 100, 0, 1500, "lep+photon energy [GeV]", "lepph_e"),
("lepph_m/1e3", 100, 0, 500, "lep+photon mass [GeV]", "lepph_m"),
("lepph_dr", 50, 0, 5, "$\Delta$R(lep, ph)", "lepph_dr"),
("lepph_dphi", 40, -3.14, 3.14, "$\Delta \phi$(lep, ph)", "lepph_dphi"),
("lepph_deta", 40, 0, 5, "$\Delta \eta$(lep, ph)", "lepph_deta"),
("lepph_ctheta", 100, -1, 1, "cos(theta)(lep, ph)", "lepph_ctheta"),
("fjph_pt/1e3", 50, 10, 310, "fj+photon $p_{T}$ [GeV]", "fjph_pt"),
("fjph_eta", 40, -5.0, 5.0, "fj+photon $\eta$", "fjph_eta"),
("fjph_phi", 30, -3.14, 3.14, "fj+photon $\phi$", "fjph_phi"),
("fjph_e/1e3", 100, 0, 1500, "fj+photon energy [GeV]", "fjph_e"),
("fjph_m/1e3", 100, 0, 500, "fj+photon mass [GeV]", "fjph_m"),
("fjph_dr", 50, 0, 5, "$\Delta$R(fj, ph)", "fjph_dr"),
("fjph_dphi", 40, -3.14, 3.14, "$\Delta \phi$(fj, ph)", "fjph_dphi"),
("fjph_deta", 40, 0, 5, "$\Delta \eta$(fj, ph)", "fjph_deta"),
("fjph_ctheta", 100, -1, 1, "cos(theta)(fj, ph)", "fjph_ctheta"),
("bph_pt/1e3", 50, 10, 310, "b+photon $p_{T}$ [GeV]", "bph_pt"),
("bph_eta", 40, -5.0, 5.0, "b+photon $\eta$", "bph_eta"),
("bph_phi", 30, -3.14, 3.14, "b+photon $\phi$", "bph_phi"),
("bph_e/1e3", 100, 0, 1500, "b+photon energy [GeV]", "bph_e"),
("bph_m/1e3", 100, 0, 500, "b+photon mass [GeV]", "bph_m"),
("bph_dr", 50, 0, 5, "$\Delta$R(b, ph)", "bph_dr"),
("bph_dphi", 40, -3.14, 3.14, "$\Delta \phi$(b, ph)", "bph_dphi"),
("bph_deta", 40, 0, 5, "$\Delta \eta$(b, ph)", "bph_deta"),
("bph_ctheta", 100, -1, 1, "cos(theta)(b, ph)", "bph_ctheta"),
("blep_pt/1e3", 50, 10, 310, "b+lepton $p_{T}$ [GeV]", "blep_pt"),
("blep_eta", 40, -5.0, 5.0, "b+lepton $\eta$", "blep_eta"),
("blep_phi", 30, -3.14, 3.14, "b+lepton $\phi$", "blep_phi"),
("blep_e/1e3", 100, 0, 1500, "b+lepton energy [GeV]", "blep_e"),
("blep_m/1e3", 100, 0, 500, "b+lepton mass [GeV]", "blep_m"),
("blep_dr", 50, 0, 5, "$\Delta$R(b, lep)", "blep_dr"),
("blep_dphi", 40, -3.14, 3.14, "$\Delta \phi$(b, lep)", "blep_dphi"),
("blep_deta", 40, 0, 5, "$\Delta \eta$(b, lep)", "blep_deta"),
("blep_ctheta", 100, -1, 1, "cos(theta)(b, lep)", "blep_ctheta"),
("bfj_pt/1e3", 50, 10, 310, "b+fjet $p_{T}$ [GeV]", "bfj_pt"),
("bfj_eta", 40, -5.0, 5.0, "b+fjet $\eta$", "bfj_eta"),
("bfj_phi", 30, -3.14, 3.14, "b+fjet $\phi$", "bfj_phi"),
("bfj_e/1e3", 100, 0, 1500, "b+fjet energy [GeV]", "bfj_e"),
("bfj_m/1e3", 100, 0, 500, "b+fjet mass [GeV]", "bfj_m"),
("bfj_dr", 50, 0, 5, "$\Delta$R(b, fj)", "bfj_dr"),
("bfj_dphi", 40, -3.14, 3.14, "$\Delta \phi$(b, fj)", "bfj_dphi"),
("bfj_deta", 40, 0, 5, "$\Delta \eta$(b, fj)", "bfj_deta"),
("bfj_ctheta", 100, -1, 1, "cos(theta)(b, fj)", "bfj_ctheta"),
("lfj_pt/1e3", 50, 10, 310, "lep+fjet $p_{T}$ [GeV]", "lfj_pt"),
("lfj_eta", 40, -5.0, 5.0, "lep+fjet $\eta$", "lfj_eta"),
("lfj_phi", 30, -3.14, 3.14, "lep+fjet $\phi$", "lfj_phi"),
("lfj_e/1e3", 100, 0, 1500, "lep+fjet energy [GeV]", "lfj_e"),
("lfj_m/1e3", 100, 0, 500, "lep+fjet mass [GeV]", "lfj_m"),
("lfj_dr", 50, 0, 5, "$\Delta$R(lep, fj)", "lfj_dr"),
("lfj_dphi", 40, -3.14, 3.14, "$\Delta \phi$(lep, fj)", "lfj_dphi"),
("lfj_deta", 40, 0, 5, "$\Delta \eta$(lep, fj)", "lfj_deta"),
("lfj_ctheta", 100, -1, 1, "cos(theta)(lep, fj)", "lfj_ctheta"),
("topfj_pt/1e3", 50, 10, 310, "top+fjet $p_{T}$ [GeV]", "topfj_pt"),
("topfj_eta", 40, -5.0, 5.0, "top+fjet $\eta$", "topfj_eta"),
("topfj_phi", 30, -3.14, 3.14, "top+fjet $\phi$", "topfj_phi"),
("topfj_e/1e3", 100, 0, 1500, "top+fjet energy [GeV]", "topfj_e"),
("topfj_m/1e3", 100, 0, 500, "top+fjet mass [GeV]", "topfj_m"),
("topfj_dr", 50, 0, 5, "$\Delta$R(top, fj)", "topfj_dr"),
("topfj_dphi", 40, -3.14, 3.14, "$\Delta \phi$(top, fj)", "topfj_dphi"),
("topfj_deta", 40, 0, 5, "$\Delta \eta$(top, fj)", "topfj_deta"),
("topfj_ctheta", 100, -1, 1, "cos(theta)(top, fj)", "topfj_ctheta"),
("top_rap", 40, -5.0, 5.0, "top quark rapidity (y)", "top_rap"),
("Wbsn_rap", 40, -5.0, 5.0, "W boson rapidity (y)", "Wbsn_rap"),
("topph_rap", 40, -5.0, 5.0, "(top+photon) rapidity (y)", "topph_rap"),
("Wph_rap", 40, -5.0, 5.0, "(W+photon) rapidity (y)", "Wph_rap"),
("lepph_rap", 40, -5.0, 5.0, "(lepton+photon) rapidity (y)", "lepph_rap"),
("bph_rap", 40, -5.0, 5.0, "(b-jet+photon) rapidity (y)", "bph_rap"),
("blep_rap", 40, -5.0, 5.0, "(b-jet+lepton) rapidity (y)", "blep_rap"),
]


if __name__ == "__main__":
    import sys
    opath = "/data/harish/TensorPlots/v1/"
    df = pd.read_csv("/data_ceph/harish/tqGamma/csv_v3/16Jun2020v1.csv")
    df.keys()
    i=0
    for key in df.keys():
        found = False
        for var in varlist:
            if (var[0]==key):
                found = True
                break
        if(not found):
            print("Key: %s not found \n"%key)
    #sys.exit(1)
    for var in varlist:
        if (var[0] not in df.keys()):
            print("Key Not Found in the dataframe: %s" %var[0])
            continue
        print(var[0], var[1], var[2], var[3], var[4], var[5])
        stackHist(df, var[0], var[1], var[2], var[3], var[4], var[5], opath=opath)
        #binaryHist(df, var[0], var[1], var[2], var[3], var[4], var[5], opath=opath)
        continue
        if(i%2 == 0):
            print("\end{frame}")
            print("\\begin{frame}{}")
        print("\includegraphics[width=.5\linewidth]{%s}"%var[5])
        i+=1
    



