# Run on Jupyter Terminal to avoid clash between python3 and ROOT

import numpy as np
import ROOT, tqdm, root_numpy
from enum import Enum
import pandas as pd

class Category(Enum):
    tqGamma = 1
    ttgamma = 0
    Wgamma = 0
    Zgamma = 0
    ttbar = 0
    Diboson = 0
    schan = 0
    tchan = 0
    Wjets = 0
    Zjets = 0
    others = 0

class ProcName(Enum):
    tqGamma = 1
    ttgamma = 2
    Wgamma = 3
    Zgamma = 4
    ttbar = 5
    Diboson = 6
    schan = 7
    tchan = 8
    Wjets = 9
    Zjets = 10
    others = 11

inpath = "/data_ceph/harish/16Jun2020/merged/"
outpath = "/data_ceph/harish/tqGamma/csv_v3/"
fnames = ["tqGamma", "ttgamma", "Wgamma", "Zgamma", "ttbar", "Diboson", "schan", "tchan", "Wjets", "Zjets"]

cut="nlep==1&&nph>0&&lep1_pt>27e3&&ph_pt>20e3&&lbj_pt>25e3&&met_met>30e3&&nbjets_DL1r_70==1&&nbjets_DL1r_85==1&&(lepph_m<80e3||lepph_m>100e3||abs(lep1_id)!=11)"

weightVar = "weight_mc*weight_leptonSF*weight_pileup*scale_nom*ph_sf_id*ph_sf_iso*lumi*weight_bTagSF_DL1r_Continuous*efakeweight*weight_jvt*hfakeweight"

varlist = ["lep1_pt/1e3", "lep1_eta", "lep1_id", "lep1_phi", "ph_pt/1e3", "ph_eta","ph_phi" ,"ph_conversionType", "met_met/1e3", "met_phi", 
           "lj_pt/1e3", "lj_eta", "lj_phi", "lbj_pt/1e3", "lbj_eta", "lbj_phi", 
           "fj_pt/1e3", "fj_phi", "fj_eta", "fjet_flag", "nbjets_DL1r_85", "nbjets_DL1r_77", "lbj_tagWeightBin_DL1r_Continuous",
           "njets", "nfjets", "transMass/1e3", "transMassWph/1e3", "transMassWb/1e3", "HT/1e3",
           "Wbsn_pt/1e3", "Wbsn_eta", "Wbsn_phi", "Wbsn_m/1e3", "Wbsn_e/1e3", "Wbsn_dr", "Wbsn_ctheta", "Wbsn_dphi", "Wbsn_deta",
           "top_pt/1e3", "top_eta", "top_phi", "top_m/1e3", "top_e/1e3", "top_dr", "top_ctheta", "top_dphi", "top_deta",
           "topph_pt/1e3", "topph_eta", "topph_phi", "topph_m/1e3", "topph_e/1e3", "topph_dr", "topph_ctheta", "topph_dphi", "topph_deta",
           "Wph_pt/1e3", "Wph_eta","Wph_phi", "Wph_m/1e3", "Wph_e/1e3", "Wph_dr", "Wph_ctheta", "Wph_dphi", "Wph_deta",
           "lepph_pt/1e3", "lepph_eta","lepph_phi", "lepph_m/1e3", "lepph_e/1e3", "lepph_dr", "lepph_ctheta", "lepph_dphi", "lepph_deta",
           "fjph_pt/1e3", "fjph_eta", "fjph_phi", "fjph_m/1e3", "fjph_e/1e3", "fjph_dr", "fjph_ctheta", "fjph_dphi", "fjph_deta",
           "bph_pt/1e3", "bph_eta", "bph_phi", "bph_m/1e3", "bph_e/1e3", "bph_dr", "bph_ctheta", "bph_dphi", "bph_deta",
           "blep_pt/1e3", "blep_eta", "blep_phi", "blep_m/1e3", "blep_e/1e3", "blep_dr", "blep_ctheta", "blep_dphi", "blep_deta",
           "bfj_pt/1e3", "bfj_eta", "bfj_phi", "bfj_m/1e3", "bfj_e/1e3", "bfj_dr", "bfj_ctheta", "bfj_dphi", "bfj_deta",
           "lfj_pt/1e3", "lfj_eta", "lfj_phi", "lfj_m/1e3", "lfj_e/1e3", "lfj_dr", "lfj_ctheta", "lfj_dphi", "lfj_deta",
           "topfj_pt/1e3", "topfj_eta", "topfj_phi", "topfj_m/1e3", "topfj_e/1e3", "topfj_dr", "topfj_ctheta", "topfj_dphi", "topfj_deta",
           "top_rap", "Wbsn_rap", "topph_rap", "Wph_rap", "lepph_rap", "bph_rap", "blep_rap", 
           "eventNumber", "runNumber",
           weightVar]

allMC = []
raw_data = []
vardict = {}

for i in range(len(varlist)-1):
    vardict[varlist[i]] = []

vardict["mcWeight"] = []
vardict["category"] = []
vardict["procName"] = []
for f in fnames:
    print(f)
    tf = ROOT.TFile().Open(inpath+f+".root")
    tree = tf.nominal
    raw_data = root_numpy.tree2array(tree, varlist, selection=cut)

    for i in tqdm.tqdm(range(len(raw_data))):
        for j in range(len(varlist)-1):
            vardict[varlist[j]].append(np.asscalar(raw_data[i][j]))
        vardict["mcWeight"].append(raw_data[i][len(varlist)-1])
        vardict["category"].append(getattr(Category, f).value)
        vardict["procName"].append(getattr(ProcName, f).value)

df = pd.DataFrame.from_dict(vardict)
df.to_csv(outpath+"/16Jun2020v2.csv")
