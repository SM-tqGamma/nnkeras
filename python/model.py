import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import tensorflow as tf
from tensorflow import keras
import math, json, pickle, h5py
from plotTensors import stackHist, binaryHist, varlist
from sklearn.metrics import confusion_matrix, classification_report, roc_curve, roc_auc_score
import os

np.random.seed(34532)

def auc(y_true, y_pred):
    auc = tf.metrics.auc(y_true, y_pred)[0]
    keras.backend.get_session.run(tf.local_variable_initializer())
    return auc

class BinaryClassifier:
    def __init__(self, N_input=0, width=0, depth=0):
        self.N_input = N_input
        self.width = width
        self.depth = depth
        self.metrics = [keras.metrics.BinaryAccuracy(name='accuracy'), keras.metrics.AUC(name="auc")]
        self.wgtmetrics = [keras.metrics.BinaryAccuracy(name='wgt_accuracy'), keras.metrics.AUC(name="wgt_auc")]

    def Build(self, alpha=0.05):
        model = keras.models.Sequential()
        model.add(keras.layers.Dense(self.N_input+1,input_dim=self.N_input))
        model.add(keras.layers.LeakyReLU(alpha=alpha))
        for i in range(0, self.depth):
            model.add(keras.layers.Dense(self.width))
            model.add(keras.layers.LeakyReLU(alpha=alpha))
            #model.add(Dropout=0.5)
        model.add(keras.layers.Dense(1, activation="sigmoid"))
        self.Model = model

    def Compile(self, opt="", loss="", lrinit=1.e-5):
        optimizer = keras.optimizers.SGD(lr=0.001, decay=1e-6, momentum=0.9,nesterov=True)
        if opt == "adam":
            optimizer = keras.optimizers.Adam(lr=lrinit, beta_1=0.99, beta_2=0.999, epsilon=1e-08)
        
        self.Model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=self.metrics, weighted_metrics=self.wgtmetrics)

def plotMetric(history, metric, opath, title=""):
    plt.figure()
    plt.plot(history[metric])
    plt.plot(history["val_"+metric])
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend(["Training", "Validation"], loc="lower right")
    plt.title(title)
    plt.savefig(opath+metric+".pdf", bbox_inches='tight')
    plt.close()
    return

def printSigOverBkg(df, label=""):
    weights_sig = df.loc[df["category"] == 1]["mcWeight"].values
    weights_bkg = df.loc[df["category"] == 0]["mcWeight"].values
    print("Statistics for %s"%label)
    print("Number of events: sig: %s  ; bkg: %s" % (len(weights_sig), len(weights_bkg)))
    print("SumWeights  sig: %8.2f  ;  bkg: %8.2f" %(np.sum(weights_sig), np.sum(weights_bkg)) )
    print("Ratio S/sqrt(B): ", str( np.sum(weights_sig)/ np.sqrt(np.sum(weights_bkg)) ) )
    return

def normalize(df, featurelist, opath): # Should I use df.mean()/std() instead of Min/Max?
    result = df.copy(deep=True)
    exclude_list = ['Unnamed: 0', "eventNumber", "runNumber", "mcWeight", "procName", "category"]
    MinMaxDict = {}

    for key in featurelist:
        if key in exclude_list:
            print("Key: %s excluded from the normalization" %key)
            continue
        max_value = df[key].max()
        min_value = df[key].min()
        MinMaxDict[key] = [min_value, max_value]
        #print("%s  Min: %8.2f   Max: %8.2f  " %(key, max_value, min_value))
        result[key] = ( (df[key]-min_value) / (max_value - min_value) )
    MinMaxDF = pd.DataFrame.from_dict(MinMaxDict)
    MinMaxDF.to_csv(opath+"MinMax.csv")
    return result

    for var in varlist:
        if(var[0] not in featurelist):
            continue
        xlo = (var[2]-MinMaxDF[var[0]][0]) / (MinMaxDF[var[0]][1] - MinMaxDF[var[0]][0])
        xhi  = (var[3]-MinMaxDF[var[0]][0]) / (MinMaxDF[var[0]][1] - MinMaxDF[var[0]][0])
        stackHist(result, var[0], var[1], xlo, xhi, var[4], var[5], opath=opath+"inputs/")
        binaryHist(result, var[0], var[1], xlo, xhi, var[4], var[5], density=True, opath=opath+"inputs/")
    return result

def reweight_events(inputdf):
    df = inputdf.copy(deep=True)
    sig_wgt = df[df["category"] == 1]["mcWeight"].sum()
    bkg_wgt = df[df["category"] == 0]["mcWeight"].sum()
    ratio = bkg_wgt/sig_wgt
    print("Sum of signal, bkg weights and ratio: %8.2f %8.2f and %8.2f" %(sig_wgt, bkg_wgt, ratio))
    df.loc[df["category"]==1, "mcWeight"]= df.loc[df["category"]==1, "mcWeight"].mul(ratio)
    print("Sum of Weights after normalizing: ", df[df["category"] == 1]["mcWeight"].sum(), df[df["category"] == 0]["mcWeight"].sum())
    return df

def df2array(df, featurelist, sig_frac=1.0, bkg_scale=-1, random_state=45, match_weights=False):
    signal = df[df["category"]==1]
    bkg    = df[df["category"]==0]
    df_sig = signal.sample(frac=sig_frac,random_state=random_state)
    if(bkg_scale==-1):
        df_bkg = bkg.sample(frac=1.0, random_state=random_state)
    elif (bkg_scale > 1.0):
        df_bkg = bkg.sample(n=int(len(df_sig)*bkg_scale), random_state=random_state)
    df_all = df_sig.append(df_bkg, ignore_index=True)
    df_all = df_all.sample(frac=1.0, random_state=random_state)
    X_all = np.asarray( df_all[featurelist].values )
    Y_all = np.asarray ( df_all["category"].values )
    weights_all = np.asarray ( df_all["mcWeight"].values )
    scale = np.sum(weights_all[Y_all==0])/np.sum(weights_all[Y_all==1])
    if(not match_weights):
        scale = 1.0
    weights_all[Y_all==1] = weights_all[Y_all ==1]*scale
    return X_all, Y_all, weights_all

def getArrays(df, featurelist, random_state=45):
    df = df.sample(frac=1.0, random_state=random_state)
    X_all = np.asarray( df[featurelist].values )
    Y_all = np.asarray ( df["category"].values )
    weights_all = np.asarray ( df["mcWeight"].values )
    return X_all, Y_all, weights_all


def plotPerformance(df, featurelist, model, label="test",cutoff=0.5, title=""):
    X = np.asarray(df[featurelist].values)
    Y = np.asarray(df["category"].values)
    weights = np.asarray(df["mcWeight"].values)
    Y_pred1 = model.predict(X)
    df["pred"] = Y_pred1
    stackHist(df, "pred", 20, 0, 1, "Neural network output", "nnoutput_"+label, opath=opath)
    Y_pred = np.asarray([x[0] for x in Y_pred1])
    Y_pred2 = np.where(Y_pred>cutoff, 1, 0)
    cm = confusion_matrix(Y, Y_pred2, sample_weight=weights)
    print("\n Confusion Matrix \n", cm , "\n")
    plt.figure()
    ax = plt.subplot()
    sns.heatmap(cm, annot=True, ax=ax)
    ax.set_xlabel("Predicted labels")
    ax.set_ylabel("True labels")
    ax.set_title("Confusion Matrix")
    ax.xaxis.set_ticklabels(["Background", "Signal"])
    ax.yaxis.set_ticklabels(["Background", "Signal"])
    plt.savefig(opath+"confusion_matrix_"+label+".pdf", bbox_inches='tight')
    fpr, tpr, thresholds = roc_curve(Y, Y_pred1, sample_weight=weights)
    #fpr2, tpr2, thresholds2 = roc_curve(Y, Y_pred1)
    roc_auc = roc_auc_score(Y, Y_pred1)#, sample_weight=weights)    
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange', lw=lw, label="ROC curve without MC weights (area = %0.2f)" % roc_auc)
    plt.plot([0,1], [0,1], color='navy', lw=lw, linestyle="--")
    plt.xlim([0.0,1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title(title)
    plt.legend(loc="lower right")
    plt.savefig(opath+"roc_curve_"+label+".pdf", bbox_inches="tight")
    return
    #print(classification_report(Y, Y_pred1, labels=["Background", "Signal"]))

class EarlyStoppingAtMinValLoss(keras.callbacks.Callback):
    def __init__(self, patience=500):
        super(EarlyStoppingAtMinValLoss, self).__init__()
        self.patience = patience 
        self.best_weights = None
        self.epoch_number = 0

    def on_train_begin(self, logs=None):
        self.wait = 0
        self.stopped_epoch = 0
        self.best = 0.0

    def on_epoch_end(self, epoch, logs=None):
        self.epoch_number += 1
        current = logs.get("val_wgt_auc")
        print(current)
        if(self.epoch_number % 500 == 0):
            pass
            #self.model.save(opath+"model_at_epoch_%s"%(str(self.epoch_number)))
        if np.less(self.best+0.0001, current):
            self.best = current
            self.wait = 0
            self.best_weights = self.model.get_weights()
        elif self.best > 0.60:
            self.wait += 1
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
                print("Restoring model weights from the end of the best epoch.\n")
                self.current_weights = self.model.get_weights()
                self.model.set_weights(self.best_weights)
                self.model.save(opath+"best_min_val_loss_model")

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0:
            print("Epoch %05d: early stopping" % (self.stopped_epoch + 1))
        


import sys
if(len(sys.argv)>0):
    channel = int(sys.argv[1])

opath = "/data/harish/NNResults/Aug24/" #"/work/03860/hpotti/maverick2/Aug23/"
lbl = None

fdf = pd.read_csv("/data_ceph/harish/tqGamma/csv_v3/16Jun2020v1.csv")
#fdf = pd.read_csv("/work/03860/hpotti/maverick2/csv_v3/16Jun2020v1.csv")
fdf.keys()

fdf = fdf[ (fdf["nbjets_DL1r_85"]==1) & (fdf["ph_pt/1e3"]>20) ]
fdf.loc[fdf.njets > 4, "njets"] = 4
fdf.loc[fdf.nfjets > 4, "nfjets"] = 4

df = None

featurelist1 = ['lep1_pt/1e3', 'lep1_eta', 'lep1_id', 'lep1_phi',
       'ph_pt/1e3', 'ph_eta', 'ph_phi', 'met_met/1e3', "HT/1e3", 
       'met_phi', 'lbj_pt/1e3',
       'lbj_eta', 'lbj_phi', 'njets',
        'fj_pt/1e3', 'fj_phi', 'fj_eta', 'nfjets',
       'transMass/1e3', "fjph_ctheta", 'fjet_flag', "bfj_m/1e3", "blep_m/1e3", 
                "bph_m/1e3", 'transMassWb/1e3', "transMassWph/1e3", "lbj_tagWeightBin_DL1r_Continuous"] # remove ph_conversionType

featurelist3 = ['lep1_pt/1e3', 'lep1_eta', 'lep1_id', 'lep1_phi',
       'ph_pt/1e3', 'ph_eta', 'ph_phi', 'met_met/1e3', "HT/1e3",
       'met_phi', 'lbj_pt/1e3',
       'lbj_eta', 'lbj_phi', 'njets',
                'fj_pt/1e3', 'fj_phi', 'fj_eta', 'nfjets', "fjet_flag", "lbj_tagWeightBin_DL1r_Continuous"]

#       'transMass/1e3', "fjph_ctheta", 'fjet_flag', "bfj_m/1e3", "blep_m/1e3",
 #                           "bph_m/1e3", 'transMassWb/1e3', "transMassWph/1e3", "lbj_tagWeightBin_DL1r_Continuous"] 

featurelist4 = [ 'njets', "bfj_m/1e3", "lepph_m/1e3", "top_m/1e3",
                 'transMass/1e3', "fjph_ctheta", 'lep1_id',
                 "HT/1e3", "blep_m/1e3", 'fj_pt/1e3', "fjph_deta", "bph_m/1e3",
                 "bph_deta", "fjph_e/1e3", "fjet_flag", "blep_deta", "nfjets",
                 "Wbsn_e/1e3", "lbj_tagWeightBin_DL1r_Continuous" ]

featurelist2 = ['lep1_pt/1e3', 'lep1_eta', 'lep1_id', 'lep1_phi',
       'ph_pt/1e3', 'ph_eta', 'ph_phi', 'met_met/1e3', "HT/1e3", 
       'met_phi', 'lbj_pt/1e3',
       'lbj_eta', 'lbj_phi', 'njets', 
        'transMass/1e3', "blep_m/1e3", "bph_m/1e3", "blep_rap", "topph_ctheta",
                'transMassWb/1e3', "transMassWph/1e3", "lbj_tagWeightBin_DL1r_Continuous"] 

featurelist5 = ['lep1_pt/1e3', 'lep1_eta', 'lep1_id', 'lep1_phi',
       'ph_pt/1e3', 'ph_eta', 'ph_phi', 'met_met/1e3', "HT/1e3",
       'met_phi', 'lbj_pt/1e3',
       'lbj_eta', 'lbj_phi', 'njets', "lbj_tagWeightBin_DL1r_Continuous" ]
#        'transMass/1e3', "blep_m/1e3", "bph_m/1e3", "blep_rap", "topph_ctheta",
 #               'transMassWb/1e3', "transMassWph/1e3", "lbj_tagWeightBin_DL1r_Continuous"]

featurelist6 = [ 'lbj_eta', 'njets', "bph_deta", 'transMass/1e3',
                 "top_m/1e3", "bph_pt/1e3",
                 "blep_m/1e3", "lbj_tagWeightBin_DL1r_Continuous", "bph_m/1e3",
                 'lbj_pt/1e3', "topph_pt/1e3", 'lep1_id', "blep_deta",
                 'met_met/1e3', "blep_dr"]
#    'lep1_pt/1e3', 'lep1_eta', 'lep1_id', 'lep1_phi',
#       'ph_pt/1e3', 'ph_eta', 'ph_phi', 'met_met/1e3', "HT/1e3",
 #      'met_phi', 'lbj_pt/1e3',
  #     'lbj_phi', 
   #     'transMass/1e3', "blep_m/1e3", "blep_rap", "topph_ctheta",
    #            'transMassWb/1e3', "transMassWph/1e3"]



featurelist = None
title = ""

if(channel==1):
    df = fdf[ (fdf["nfjets"]>0) & (fdf["ph_conversionType"]==0) ]
    lbl = "unconv1fj_complex/"
    featurelist = featurelist4
    title = "At least 1-forward jet & Unconverted photon"
elif(channel==2):
    df = fdf[ (fdf["nfjets"]>0) & (fdf["ph_conversionType"]!=0) ]
    lbl = "conv1fj/"
    featurelist = featurelist1
    title = "At least 1-forward jet & Converted photon"
elif(channel==3):
    df = fdf[ (fdf["nfjets"]==0) & (fdf["ph_conversionType"]==0) ]
    lbl = "unconv0fj/"
    featurelist = featurelist2
    title = "0 forward jets & Unconverted photon"
elif(channel==4):
    df = fdf[ (fdf["nfjets"]==0) & (fdf["ph_conversionType"]!=0) ]
    lbl = "conv0fj/"
    featurelist = featurelist2
    title = "0 forward jets & Converted photon"
elif(channel==5):
    df = fdf[ fdf["nfjets"]==0 ]
    lbl = "0-fj/"
    featurelist = featurelist6
    title = "Zero forward jets"
elif(channel==6):
    df = fdf[ fdf["nfjets"]>0 ]
    lbl = "1-fj/"
    featurelist = featurelist4
    title = "At least 1 forward jet"
else:
    print("Invalid channel")
    sys.exit(1)

print(title, "\n", featurelist)
opath += lbl
os.system("mkdir -p %s" % opath)


df = df.sample(frac=1.)
printSigOverBkg(df, "All Events")

# Normalize and plot

pd.options.mode.use_inf_as_na = True
df_nan = df[df.isna().any(axis=1)]
print("Removing %s entries with nan or inf values" %len(df_nan))
df = df.dropna()

norm_df = normalize(df, featurelist, opath=opath)
rwgt_df = reweight_events(norm_df)

df_train = rwgt_df.sample(frac=0.50, random_state=514)
df_valtest = rwgt_df.drop(df_train.index)
df_val = df_valtest.sample(frac=0.5, random_state=231)
df_test = df_valtest.drop(df_val.index)
printSigOverBkg(df_train, "Training")
printSigOverBkg(df_val, "Validation")
printSigOverBkg(df_test, "Testing")

X_train, Y_train, weights_train = getArrays(df_train, featurelist, random_state=45)
X_val, Y_val, weights_val = getArrays(df_val, featurelist, random_state=45)
#wratio = np.sum(weights_train)/ np.sum(weights_val)

#weights_val = wratio*weights_val
print("Sum of weights for training: %8.2f  ; validation:  %8.2f" % (np.sum(weights_train), np.sum(weights_val)) )

#model = getModel(len(featurelist))
classifier = BinaryClassifier(N_input=len(featurelist), depth=2, width=len(featurelist)+1)
classifier.Build()
classifier.Compile()
model = classifier.Model #keras.utils.multi_gpu_model(classifier.Model, gpus=3)

history = model.fit(X_train, Y_train, 
                    sample_weight=weights_train, 
                    validation_data=(X_val, Y_val, weights_val), epochs=5000, batch_size=10000, shuffle=True,
                    callbacks=[EarlyStoppingAtMinValLoss()])

model_json = model.to_json()
with open(opath+"NNet.json", "w") as json_file:
    json_file.write(model_json)

model.save(opath+"NNnet.h5")
model.save_weights(opath+"model_finalweights.h5")

pickle.dump(history.history, open(opath+"history.pickle", "wb"))
with open(opath+"history.json", "w") as f:
    json.dump(str(history.history), f)

metrics = ["loss", "accuracy", "auc", "wgt_accuracy", "wgt_auc"]
for metric in metrics:
    plotMetric(history.history, metric, opath=opath, title=title)

plotPerformance(df_test, featurelist, model, label="test",cutoff=0.5, title=title)
plotPerformance(norm_df, featurelist, model, label="all",cutoff=0.5, title=title)

