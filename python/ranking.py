import matplotlib
matplotlib.use('Agg')

import numpy as np
import pandas as pd
import xgboost as xgb
import matplotlib.pyplot as plt
import sklearn
from plotTensors import varlist as varlist
import sys, pickle

opath = "/data/harish/NNResults/29Jun2020/xgboost/"

fdf = pd.read_csv("/data_ceph/harish/tqGamma/csv_v3/16Jun2020v1.csv")
fdf.keys()
fdf = fdf[ (fdf["nbjets_DL1r_85"]==1) & (fdf["ph_pt/1e3"]>20) ]
fdf.loc[fdf.njets > 4, "njets"] = 4
fdf.loc[fdf.nfjets > 4, "nfjets"] = 4

fdf.loc[fdf.ph_conversionType != 0, "ph_conversionType"] = 1

def RankFeatures(df, features, feature_names, fname):
    print("Number of Features: ", len(features))
    print(features, "\n", feature_names)
    
    train, test = sklearn.model_selection.train_test_split(df, test_size=0.3, shuffle=True, random_state=51, stratify=df[["procName"]])
    
    y_train = train["category"]
    y_test = test["category"]
    
    weights_train = train["mcWeight"]
    weights_test = test["mcWeight"]

    train = train[features]
    test = test[features]

    xgb_train = xgb.DMatrix(train, label=y_train, weight=weights_train, feature_names=feature_names)
    xgb_test = xgb.DMatrix(test, label=y_test, weight=weights_test, feature_names=feature_names)

    params = {
        'learning_rate' : 0.01,
        'max_depth': 9,
        'min_child_weight': 2,
        'gamma': 0.9,
        'subsample' : 0.8,
        'colsample_bytree' : 0.8,
        'eval_metric': 'auc',
        'nthread': -1,
        'scale_pos_weight':1
    }

    gbm = xgb.cv(params, xgb_train, num_boost_round=500, verbose_eval=True)
    
    best_nrounds = gbm['test-auc-mean'].idxmax()
    print("Training and Testing Accuracy: ", best_nrounds, gbm['train-auc-mean'][best_nrounds], gbm['test-auc-mean'][best_nrounds])
    
    bst = xgb.train(params, xgb_train, num_boost_round=best_nrounds, verbose_eval=True)
    pickle.dump(bst, open(opath+fname+"_xgbmodel.dat", "wb"))
    
    y_test_pred = bst.predict(xgb_test)
    y_train_pred = bst.predict(xgb_train)
    
    plt.figure(figsize=(10,20))
    fip = xgb.plot_importance(bst, max_num_features=20)
    plt.title("xgboost feature important")
    plt.legend(loc='lower right')
    plt.savefig(opath+'/rank_'+fname+'.pdf',  bbox_inches='tight')
    plt.close()
    
    fi = bst.get_fscore()
    plt.figure(figsize=(30,30))
    plt.barh(range(len(fi)), sorted(fi.values()), align = "center")
    plt.yticks(range(len(fi)), sorted(fi, key=fi.get))
    plt.xlabel('F-score')
    plt.ylabel("Features")
    plt.title("Feature importance")
    plt.savefig(opath+'/rank_'+fname+'_hbar.pdf',  bbox_inches='tight')

    #return gbm

if __name__ == "__main__":
    vlist = ""
    fname = ""
    if(len(sys.argv)> 3):
        nf = int(sys.argv[1])
        randint = int(sys.argv[2])
        fname = sys.argv[3]
        frac = nf*1.0/len(varlist)
        print(frac)
        discard, vlist = sklearn.model_selection.train_test_split(varlist, test_size=0.2, shuffle=True, random_state=randint)
    else:
        fname = "all"
        vlist = varlist
    features = [var[0] for var in vlist]
    feature_names = [var[4].replace(" [GeV]", "") for var in vlist]
    print(features, len(features))
    #RankFeatures(fdf, features, feature_names, fname)
    fdf_0fj = fdf[fdf["nfjets"]==0]
    fdf_1fj = fdf[fdf["nfjets"]>0]
    RankFeatures(fdf_0fj, features, feature_names, fname+"0fj")
    RankFeatures(fdf_1fj, features, feature_names, fname+"1fj")
