import os,sys, ROOT
from tqdm import tqdm
import click
from tensorflow import keras
import numpy as np
import pandas as pd
import multiprocessing as mp
from functools import partial

def getNNout(tree, channel):
    MinMaxDF_1fj = pd.read_csv("../input/chosen/1fj_nom/MinMax.csv")
    MinMaxDF_0fj = pd.read_csv("../input/chosen/0fj_nom/MinMax.csv")
    featurelist_1fj = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', \
    'ph_pt', 'ph_eta', 'ph_phi', 'met_met', "HT",\
    'met_phi', 'lbj_pt','lbj_eta', 'lbj_phi', 'njets', \
    'fj_pt', 'fj_phi', 'fj_eta', 'nfjets','transMass', "fjph_ctheta",\
    'fjet_flag', "bfj_m", "blep_m","bph_m", 'transMassWb', \
    "transMassWph", "lepph_dr", "lfj_dr", "blep_dr", "fjph_dr", "fjph_m", "top_m", "Wbsn_e", "topph_pt","topph_ctheta", "fjph_e", "fjph_deta", "bph_deta", "blep_deta",\
    "ph_conversionType", "lbj_tagWeightBin_DL1r_Continuous", "lepph_m", "bph_pt"]

    featurelist_0fj = ['lep1_pt', 'lep1_eta', 'lep1_id', 'lep1_phi', \
    'ph_pt', 'ph_eta', 'ph_phi', 'met_met', "HT",\
    'met_phi', 'lbj_pt','lbj_eta', 'lbj_phi', 'njets', \
    'transMass', "blep_m","bph_m", 'transMassWb', \
    "transMassWph", "lepph_dr",  "blep_dr", "top_m", "Wbsn_e", "topph_pt","topph_ctheta","bph_deta", "blep_deta","ph_conversionType", "lbj_tagWeightBin_DL1r_Continuous", "lepph_m", "bph_pt"]

    remvars_1fj = ["ph_conversionType","topph_pt", "topph_ctheta", "lepph_m", "bph_deta", "bph_m", "ph_eta", "fj_phi", "lbj_phi", "lfj_dr", "bph_pt", "lbj_tagWeightBin_DL1r_Continuous", "transMassWph", "lbj_pt", "ph_pt", "HT", "blep_deta"]
    remvars_0fj = ["ph_conversionType", "lbj_phi", "bph_pt", "lepph_m", "lep1_id", "blep_deta", "Wbsn_e", "transMass", "topph_ctheta", "bph_deta", "lep1_phi", "lbj_pt", "met_phi", "ph_phi", "met_met"]

'''
woNjets
remvars_1fj = ["njets", "nfjets", "ph_conversionType", 'bph_deta', 'bph_pt', 'topph_ctheta', 'bph_m', 'lep1_phi', 'lepph_m', 'met_phi', 'blep_deta', 'ph_eta', 'lbj_phi', 'fjph_m', 'fjet_flag', 'met_met', 'fj_eta', 'fj_phi', 'transMassWb', 'fjph_ctheta', 'ph_phi', 'lbj_eta', 'topph_pt', 'ph_pt', 'Wbsn_e', 'fjph_dr', 'lep1_eta', 'lep1_pt']    #for 1fj
remvars_0fj = ["njets", "ph_conversionType",'bph_pt', 'lep1_id', 'met_phi', 'lepph_m', 'blep_deta', 'ph_pt', 'bph_deta', 'lbj_phi', 'Wbsn_e', 'bph_m', 'transMass', 'topph_ctheta', 'lep1_phi', 'ph_phi', 'transMassWb', 'blep_m', 'met_met']  #for 0fj 

Mixing:
remvars_1fj = ["njets", "nfjets","ph_conversionType", "blep_deta", "lepph_m", "topph_pt", "lep1_phi", "transMass", "ph_eta", "lep1_id", "bph_pt", "fj_pt", "bph_deta", "transMassWph", "lfj_dr", "lepph_dr"]
remvars_0fj = ["njets","ph_conversionType", "lep1_pt", "transMass", "blep_deta", "lep1_phi", "ph_phi", "bph_deta", "Wbsn_e", "topph_ctheta", "lepph_m", "blep_m", "met_phi", "lbj_phi", "bph_m"]
'''
    
    featurelist_1fj = [feature for feature in featurelist_1fj if not feature in remvars_1fj]
    featurelist_0fj = [feature for feature in featurelist_0fj if not feature in remvars_0fj]

    featurelist = featurelist_0fj
    df = MinMaxDF_0fj
    if "1fj" == channel: 
        featurelist = featurelist_1fj
        df = MinMaxDF_1fj
    feats = []
    
    for feature in featurelist: 
        scale = 1.
        if feature.split("_")[-1] in ["e", "m", "met", "pt"] or "Mass" in feature or "HT" in feature: scale = 1000. 
        exec(f'feats.append((tree.{feature}*{scale} - df["{feature}"][0]) / (df["{feature}"][1] - df["{feature}"][0]))')
    return np.asarray([feats])

def NNout(params, model_0fj, model_1fj):
    ifile = params[0]
    ofile = params[1]

    itfile = ROOT.TFile(ifile)
    itree = itfile.Get("nominal")
    print(f"itree has {itree.GetEntries()} Entries")
    print("---------------------------------------------")
    ListOfBranches = itree.GetListOfBranches()
    for branch in ListOfBranches:
        if branch.GetName() == "NN_out":
            itree.SetBranchStatus("NN_out", 0)
            break
    otfile = ROOT.TFile(ofile,"RECREATE")
    otree = itree.CopyTree("1<2")
    
    itfile.Close()
    NN_out = np.zeros(1, dtype=float)
    NN_branch = otree.Branch("NN_out", NN_out, "NN_out/D")

    for entry in range(otree.GetEntries()):
        if (entry % 100.) == 0: print(f"Processing {entry}/{otree.GetEntries()}")
        otree.GetEntry(entry)
        if otree.nfjets > 0: NN_out[0] = model_1fj.predict(getNNout(otree, "1fj"))[0][0]
        else: NN_out[0] = model_0fj.predict(getNNout(otree, "0fj"))[0][0]
        NN_branch.Fill()
    otree.Write()
    otfile.Close()


@click.command()
@click.option("--idir", required=True)
@click.option("--odir", required=True)
def main(idir, odir):
    params = []
    for curFile in os.listdir(f"{idir}"): params.append([f"{idir}/{curFile}",f"{odir}/{curFile}"])
    model_1fj = keras.models.load_model("../input/chosen/1fj_nom/NNnet_kFold2.h5")
    model_0fj = keras.models.load_model("../input/chosen/0fj_nom/NNnet_kFold2.h5")
    for param in tqdm(params): NNout(param, model_0fj, model_1fj)


if __name__ == "__main__":
    main()
